﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LocalisationManager : MonoBehaviour
{
    public Dropdown myDropdown;


    private void Awake()
    {

        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "Scene_MainMenu_English")
        {
            myDropdown.value = 0;
            LanguageData.instance.languageSelected = 0;
        }
        else if (scene.name == "Scene_MainMenu_Japanese")
        {
            myDropdown.value = 1;
            //LanguageData.instance.languageSelected = 1;
            LanguageData.instance.languageSelected = 0;
        }

        myDropdown.onValueChanged.AddListener(delegate {
            OnLanguageSelected(myDropdown);
        });
    }


 
    public void OnLanguageSelected(Dropdown dropDown)
    {
        Debug.Log("My Drop Down value :"+ dropDown.value);
        switch(dropDown.value)
        {
            case 0:
                SceneManager.LoadScene("Scene_MainMenu_English");
                LanguageData.instance.languageSelected = 0;
                break;
            case 1:
                SceneManager.LoadScene("Scene_MainMenu_Japanese");
                LanguageData.instance.languageSelected = 1;
                break;
        }


    }
}
