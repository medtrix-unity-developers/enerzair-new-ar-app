﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerReset : MonoBehaviour
{
    public void ResetTargetTrigger(string trigger)
    {
        GetComponent<Animator>().ResetTrigger(trigger);
    }
}
