﻿using NVS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreezhalerSwitchLabels : MonoBehaviour
{
    public GameObject[] switchLabels;
    public GameObject hotspot;

    public void ShowLabels()
    {
        foreach(GameObject go in switchLabels)
        {
            go.SetActive(true);
        }

        FindObjectOfType<SlideManager>().ToggleLabels(true);
    }

    public void HideLabels()
    {
        foreach (GameObject go in switchLabels)
        {
            go.SetActive(false);
        }

        FindObjectOfType<SlideManager>().ToggleLabels(false);
    }

    public void OnEnable()
    {
        HideLabels();
    }

    public void ShowHotspot()
    {
        hotspot.SetActive(true);
    }

    public void HideHotspot()
    {
        hotspot.SetActive(false);
    }
}
