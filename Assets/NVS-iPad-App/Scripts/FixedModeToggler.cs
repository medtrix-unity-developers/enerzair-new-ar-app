﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace NVS
{
    public class FixedModeToggler : MonoBehaviour, IPointerClickHandler
    {
        int counter = 0;
        public int toggleThreshold = 5;

        public List<GameObject> showForFixedMode;

        public void OnPointerClick(PointerEventData eventData)
        {
            counter++;

            if(counter >= toggleThreshold)
            {
                counter = 0;
                PlayerPrefs.SetInt("FixedMode", IsFixedMode() ? 0 : 1);

                UpdateVisible();
            }
        }

        private void Start()
        {
            UpdateVisible();
        }

        void UpdateVisible()
        {
            foreach(GameObject go in showForFixedMode)
            {
                go.SetActive(IsFixedMode());
            }

            SlideManager sm = FindObjectOfType<SlideManager>();

            if (sm != null)
                sm.UpdateFixedMode();
        }

        public bool IsFixedMode()
        {
            return PlayerPrefs.GetInt("FixedMode", 1) == 1;
        }
    }
}