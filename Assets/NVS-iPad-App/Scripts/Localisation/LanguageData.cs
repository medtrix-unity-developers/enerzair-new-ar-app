﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LanguageData : MonoBehaviour
{
    public static LanguageData instance = null;


    public int languageSelected;

    void Awake()
    {
        if (instance == null)
            instance = this;

        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "Scene_Breezhaler_English")
        {
            languageSelected = 0;

        }
        else if(scene.name == "Scene_Breezhaler_Japanese")
        {
            //languageSelected = 1;
            languageSelected = 0;
        }

    }



}
