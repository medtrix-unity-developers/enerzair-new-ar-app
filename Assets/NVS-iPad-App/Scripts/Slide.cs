﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using NVS;

namespace NVS
{
    public class Slide : MonoBehaviour
    {
    	public NVS.SlideManager slidemgr;
        [Header("Targets")]
        public Transform slideContentHolder;
        public Transform slideContent;
        public Transform labelContent;

        [Header("Settings"), Multiline]
        public string headerText;
        [Multiline]
        public string headerSubText;
        //[LabelText("AR Capable")]
        public bool arCapable = false;
        public bool hasLabels = false;
        public int topRightIconIndex = -1;
        public bool isGestureDriven = true;
        public bool showReset = true;
        public bool showHome = true;
        public bool forceAR = false;
       

        //[LabelText("AR Full Rotation"), Header("Rotation Settings"), ShowIf("isGestureDriven")]
        public bool arFullRotation = true;
        //[ShowIf("isGestureDriven")]
        public bool limitRotation = false;
        //[ShowIf("DoRotationLimit")]
        public Vector3 rotationMin;
        //[ShowIf("DoRotationLimit")]
        public Vector3 rotationMax;

        //[Header("Zoom Settings"), ShowIf("isGestureDriven")]
        public float maxZoom = 3f;
        //[ShowIf("isGestureDriven")]
        public float minZoom = 1f;

        //Hotspot
        public GameObject[] HotSpot;

        bool DoRotationLimit
        {
            get { return isGestureDriven && limitRotation; }
        }

        public UnityEvent onSlideAppear;
        public UnityEvent onSlideDisappear;
        public UnityEvent onSlideReset;

        public bool isScrolling = false;

        public ClickableText clickableText;

        // Start is called before the first frame update
        void Start()
        {
            if (slideContent == null)
                slideContent = transform.GetChild(0);
        }

        public Bounds GetContentBounds()
        {
            Bounds fullBounds = new Bounds();
            bool first = true;

            foreach(Renderer r in GetComponentsInChildren<Renderer>())
            {
                if (first)
                {
                    fullBounds = new Bounds(r.bounds.center, r.bounds.size);
                    first = false;
                }
                else
                {
                    fullBounds.Encapsulate(r.bounds);
                }
            }


            return fullBounds;
        }

        public void OnEnable()
        {
            if(HotSpot.Length >=1 )
            {
               foreach(GameObject hotSpotGM in HotSpot)
                {
                    hotSpotGM.SetActive(true);
                }
            }
        }

        public void OnCapsuleDisplayed()
        {
            slideContent.GetChild(3).GetComponent<Animator>().enabled = true;
            slideContent.GetChild(3).GetComponent<Animator>().Play(0);
        }

        public void OnPointerDown()
        {
            //isScrolling = true;
            clickableText.OnPointerClick();
 
        }
        public void OnPointerUp()
        {
            //isScrolling = false;
            Debug.Log("Pointer up");
        }

        public void Update()
        {
            if((slidemgr.currentSlide == 6)) // && (slidemgr.currentSlide == 9))
            {
                if (!IsPointerOverUIObject())    //EventSystem.current.IsPointerOverGameObject())
                {
                    Debug.Log(isScrolling);
                    isGestureDriven = true;
                    isScrolling = false;
                }
                else
                {
                    Debug.Log(isScrolling);
                    isGestureDriven = false;
                    isScrolling = true;
                }
            }
            if ((slidemgr.currentSlide == 9)) 
            {
                if (!IsPointerOverUIObject())    
                {
                    Debug.Log(isScrolling);
                    isGestureDriven = true;
                    isScrolling = false;
                }
                else
                {
                    Debug.Log(isScrolling);
                    isGestureDriven = false;
                    isScrolling = true;
                }
            }
            if ((slidemgr.currentSlide == 7))
            {
                if (!IsPointerOverUIObject())
                {
                    Debug.Log(isScrolling);
                    isGestureDriven = true;
                    isScrolling = false;
                }
                else
                {
                    Debug.Log(isScrolling);
                    isGestureDriven = false;
                    isScrolling = true;
                }
            }
            if ((slidemgr.currentSlide == 8))
            {
                if (!IsPointerOverUIObject())
                {
                    Debug.Log(isScrolling);
                    isGestureDriven = true;
                    isScrolling = false;
                }
                else
                {
                    Debug.Log(isScrolling);
                    isGestureDriven = false;
                    isScrolling = true;
                }
            }
            if ((slidemgr.currentSlide == 11))
            {
                if (!IsPointerOverUIObject())
                {
                    Debug.Log(isScrolling);
                    isGestureDriven = true;
                    isScrolling = false;
                }
                else
                {
                    Debug.Log(isScrolling);
                    isGestureDriven = false;
                    isScrolling = true;
                }
            }
        }

        private bool IsPointerOverUIObject()
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }

    }
}