﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NVS
{
    public class ObjectFader : MonoBehaviour
    {
        [System.Serializable]
        public class MaterialSlot
        {
            public MeshRenderer meshRenderer;
            public Material normalMaterial;
            public Material transparentMaterial;
        }

        public List<MaterialSlot> rendererSlots;
        public float opacity = 1f;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            foreach(MaterialSlot s in rendererSlots)
            {
                if (s.normalMaterial == null || s.transparentMaterial == null)
                {
                    foreach(Material m in s.meshRenderer.materials)
                    {
                        Color c = m.GetColor("_Color");
                        m.SetColor("_Color", new Color(c.r, c.g, c.b, opacity));
                    }
                }
                else
                {
                    s.meshRenderer.material = opacity >= 1f ? s.normalMaterial : s.transparentMaterial;

                    if (opacity < 1f)
                    {
                        Color col = s.meshRenderer.material.GetColor("_Color");
                        s.meshRenderer.material.SetColor("_Color", new Color(col.r, col.g, col.b, opacity));
                    }
                }
            }
        }
    }
}