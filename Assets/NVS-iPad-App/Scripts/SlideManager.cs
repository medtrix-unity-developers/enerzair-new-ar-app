﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace NVS
{
    public class SlideManager : MonoBehaviour
    {
        [System.Serializable]
        public class SlideBranch
        {
            public string identifier;

            //[ListDrawerSettings(ShowPaging = false, ShowIndexLabels = true)]
            public Slide[] slides;
        }

        public List<SlideBranch> branches;
        public string defaultBranch = "device";
        public bool conferenceModeIsFixed = true;

        [System.NonSerialized]
        public bool arState = false;

        [System.NonSerialized]
        public bool labelState = false;
        public new Camera camera;

        public GameObject slideContainer;
        public XRController xrController;
        public Transform shadowPlane;

        [Header("UI Objects")]
        public GameObject btnHome;
        public GameObject btnReset;
        public GameObject arToggle;
        //public Animator arToggleAnimator;
        public GameObject btnLabels;
        public GameObject arrowLeft;
        public GameObject arrowRight;
        public GameObject[] topRightIcons;
        public TMPro.TextMeshProUGUI headerText;
        public TMPro.TextMeshProUGUI headerSubText;
        public UnityEngine.UI.Image headerBg;
        public Transform lightingGroup;
        public Image imgLabel;
        public Image imgAr;

        bool canReset = true;

        bool doFixedMode = false;

        [Header("Images")]
        public Sprite arActive;
        public Sprite arInactive;
        public Sprite lblActive;
        public Sprite lblInactive;

        [Header("Values")]
        public Vector3 staticCameraPosition;
        public Vector3 staticCameraRotation;
        public Vector3 arOffset;
        public bool doDeviceScaling = true;
        public Vector3 arStartScale = Vector3.one;
        //public float repositionThreshold = 0.1f;
        public float resetPositionSpeed = 1f;
        public AnimationCurve resetPositionCurve;

        [Header("Gesture Settings")]
        public float rotationDeadzone = 0.1f;
        public Vector3 rotationMultiplier = Vector2.one;
        public float pinchMultiplier = 1f;
        public float rotationResetSpeed = 1f;
        public AnimationCurve lerpCurve;

        [Header("AR Scripts")]
        public XRVideoController xrVideoController;
        public XRCameraController xrCameraController;

        //[Header("Debug Info"), ReadOnly]
        public int currentSlide = 0;
        //[ReadOnly]
        public string currentBranch = "device";
        public XRDetectedImageTarget.TrackingState arTrackingState;

        bool isPositioned = false;
        bool cancelTweens = false;

        bool hasBadTracking = true;

        Slide currentSlideCache;

        Vector2 singleTouchStartPos = Vector3.zero;
        bool didPassDeadzone = false;

        public GameObject rightbuttonTwo;
        public Animator slide1_Animator;

        // Start is called before the first frame update
        void Start()
        {
            staticCameraPosition = camera.transform.position;
            staticCameraRotation = camera.transform.eulerAngles;

            doFixedMode = IsFixedMode();

            StartCoroutine(DelayedShowFirstSlide());
        }

        IEnumerator DelayedShowFirstSlide()
        {
            yield return new WaitForEndOfFrame();

            ShowSlide(defaultBranch, 0);
            //ToggleAR(!Application.isEditor);
            ToggleAR(false);
        }

        public void ShowSlide(int index)
        {
            ShowSlide(currentBranch, index);
        }

        void ShowSlide(string branch, int index)
        {
            foreach(SlideBranch b in branches)
            {
                foreach(Slide s in b.slides)
                {
                    s.gameObject.SetActive(false);
                }
            }

            currentBranch = branch;
            currentSlide = index;
            currentSlideCache = GetSlide(branch, index);

            ShowSlideObject(GetSlide(branch, index));

            //arrowRight.SetActive(HasMoreSlides());
            arrowLeft.SetActive(HasLessSlides());
        }

        Slide GetSlide(string branch, int index)
        {
            return GetBranch(branch).slides[index];
        }

        SlideBranch GetBranch(string branch)
        {
            return branches.First(b => b.identifier == currentBranch);
        }

        void HideSlideObject(Slide s)
        {
            s.slideContentHolder.localScale = Vector3.one;
            s.gameObject.SetActive(false);

            if(s.onSlideDisappear != null)
                s.onSlideDisappear.Invoke();
        }

        void ShowSlideObject(Slide s)
        {
            s.slideContentHolder.localScale = Vector3.one;
            s.gameObject.SetActive(true);
            //s.transform.localPosition = Vector3.zero;
            s.slideContentHolder.localEulerAngles = Vector3.zero;

            for(int i = 0; i < topRightIcons.Length; i++)
            {
                topRightIcons[i].SetActive(i == s.topRightIconIndex);
            }

            arToggle.SetActive(s.arCapable);
            btnLabels.SetActive(s.hasLabels);
            btnReset.SetActive(s.showReset);

            if(btnHome != null)
                btnHome.SetActive(s.showHome);
            

            if (string.IsNullOrEmpty(s.headerText))
                headerText.transform.parent.gameObject.SetActive(false);
            else
            {
                headerText.transform.parent.gameObject.SetActive(true);
                headerText.SetText(s.headerText);
                Debug.Log("CURRENT SLIDE :"+ currentSlide);

                if(SceneManager.GetActiveScene().name == "Scene_Breezhaler_Japanese")
                {
                    if (currentSlide != 3 && currentSlide != 6)   //&& currentSlide != 4
                    {
                        Debug.Log("Coing inside CURRENT SLIDE :");
                        headerText.characterSpacing = -20f;
                        headerText.wordSpacing = -20f;
                    }
                    else
                    {
                        headerText.characterSpacing = 0f;
                        headerText.wordSpacing = 0f;
                    }
                }
                

                 //headerSubText.SetText(s.headerSubText);
            }

            ToggleAR(s.arCapable && (arState || s.forceAR));

            ToggleLabels(true, false);

            if(s.onSlideAppear != null)
                s.onSlideAppear.Invoke();
        }

        public Vector3 GetARScale()
        {
            if (doDeviceScaling)
                return Vector3.Lerp(Vector3.one, arStartScale, Mathf.InverseLerp(2048, 2732, Screen.width));
            else
                return arStartScale;
        }

        public void ResetRotation(bool tweenScale = true)
        {
            if (!canReset && arState)
                return;

            if (arState)
                StartCoroutine(RepositionAR(0f, true));

            StartCoroutine(LerpTransformTo(Quaternion.identity, arState ? GetARScale() : Vector3.one, tweenScale));
        }

        public void InvertARState()
        {
            ToggleAR(!arState);
        }

        public void ToggleAR(bool state)
        {
            bool isFreshState = arState != state;

            arState = state;
            imgAr.sprite = state ? arActive : arInactive;
            //arToggleImage.sprite = state ? toggleOn : toggleOff;

            Camera.main.orthographic = !arState;

            if (state)
            {
                
                //xrController.gameObject.SetActive(true);
                //xrController.DisableNativeArEngine(false); // Hack to call a method.

                /*if (!Application.isEditor)
                    xrController.Resume();*/

                //background.enabled = false;
                xrVideoController.enabled = true;
                xrCameraController.enabled = true;

                slideContainer.SetActive(false);

                //shadowPlane.gameObject.SetActive(true);

                currentSlideCache.slideContent.localPosition = new Vector3(0f, 0f, 0f);

                ResetSlide(currentSlideCache);

                currentSlideCache.slideContentHolder.localScale = GetARScale();

                StartCoroutine(RepositionAR(isFreshState ? 2f : 0f, false));
            }
            else
            {
                shadowPlane.gameObject.SetActive(false);
                //background.enabled = true;
                xrVideoController.enabled = false;
                xrCameraController.enabled = false;

                slideContainer.SetActive(true);

                //xrController.gameObject.SetActive(false);

                /*if(!Application.isEditor)
                    xrController.Pause();*/

                camera.transform.position = staticCameraPosition;
                camera.transform.eulerAngles = staticCameraRotation;

                slideContainer.transform.localPosition = slideContainer.transform.localEulerAngles = Vector3.zero;

                currentSlideCache.slideContent.localPosition = Vector3.zero;

                ResetSlide(currentSlideCache);

                isPositioned = true;
            }
        }

        void ResetSlide(Slide slide)
        {
            slide.slideContentHolder.localEulerAngles = slide.slideContentHolder.localPosition = Vector3.zero;
            //slide.transform.localPosition = Vector3.zero;
            slide.slideContentHolder.localScale = Vector3.one;

            if(slide.onSlideReset != null)
                slide.onSlideReset.Invoke();
        }

        public IEnumerator RepositionAR(float delay = 0f, bool tween = false)
        {
            if(doFixedMode)
            {
                slideContainer.SetActive(true);
                isPositioned = true;
                shadowPlane.gameObject.SetActive(false);
                MoveToCamera();
            } else { 

            if (delay > 0f)
            {
                canReset = false;
                yield return new WaitForSeconds(delay);
                canReset = true;
            }

            while (xrController.GetTrackingState().status != XRTrackingState.Status.NORMAL)
            {
                if (arState)
                    slideContainer.SetActive(false);
                else
                    break;
                yield return null;
            }

                if (arState)
                {
                    Vector3 floatingPos = Camera.main.transform.TransformPoint(arOffset);

                    shadowPlane.gameObject.SetActive(false);

                    isPositioned = true;

                    slideContainer.SetActive(true);

                    if (!tween)
                    {
                        MoveToCamera();
                    }
                    else
                    {
                        float timer = Vector3.Distance(slideContainer.transform.position, floatingPos) / resetPositionSpeed; // Todo: Change to field
                        float startTime = timer;
                        Vector3 startPos = slideContainer.transform.position;

                        while (timer >= 0f)
                        {
                            slideContainer.transform.position = Vector3.Lerp(startPos, floatingPos, resetPositionCurve.Evaluate(Mathf.InverseLerp(startTime, 0f, timer)));
                            slideContainer.transform.LookAt(new Vector3(Camera.main.transform.position.x, floatingPos.y, Camera.main.transform.position.z));
                            slideContainer.transform.Rotate(0f, 180f, 0f);
                            timer -= Time.deltaTime;
                            yield return null;
                        }
                    }
                }
            }
        }

        void MoveToCamera()
        {
            Vector3 floatingPos = Camera.main.transform.TransformPoint(arOffset);
            slideContainer.transform.position = floatingPos;
            slideContainer.transform.LookAt(new Vector3(Camera.main.transform.position.x, floatingPos.y, Camera.main.transform.position.z));
            slideContainer.transform.Rotate(0f, 180f, 0f);
        }

        public void ImageTargetSpotted(XRDetectedImageTarget target)
        {
            /*Vector3 floatingPos = target.position + ((target.rotation * Vector3.up) * arOffset);

            if (arState && (!isPositioned || IsFarAway(floatingPos)))
            {
                isPositioned = true;
                arTrackingState = target.trackingState;
                //Debug.Log("Image Target Detected");
                slideContainer.SetActive(true);
                slideContainer.transform.position = floatingPos;
                slideContainer.transform.rotation = Quaternion.identity;

                shadowPlane.transform.position = target.position;
                shadowPlane.transform.rotation = target.rotation;
            }*/
        }

        /*bool IsFarAway(Vector3 newPosition)
        {
            return Vector3.Distance(slideContainer.transform.position, newPosition) >= repositionThreshold;
        }*/

        public void InvertLabelState()
        {
            ToggleLabels(!labelState);
        }

        public Quaternion GetLookAtCamera()
        {
            return arState ? Quaternion.LookRotation(new Vector3(Camera.main.transform.position.x, Camera.main.transform.TransformPoint(arOffset).y, Camera.main.transform.position.z) - Camera.main.transform.TransformPoint(arOffset)) : Quaternion.identity;
        }

        void UpdateLabelSprite(bool toggle)
        {
            imgLabel.sprite = toggle ? lblActive : lblInactive;
        }

        public void ToggleLabels(bool toggle, bool tweenScale = true)
        {
            labelState = toggle;

            UpdateLabelSprite(toggle);

            StartCoroutine(ToggleLabelsAsync(toggle, tweenScale));
        }

        public IEnumerator ToggleLabelsAsync(bool toggle, bool tweenScale = true)
        {
            if(toggle)
            {
                yield return LerpTransformTo(Quaternion.identity, arState ? GetARScale() : Vector3.one, tweenScale);
            }

            if (!cancelTweens && currentSlideCache != null && currentSlideCache.labelContent != null)
                currentSlideCache.labelContent.gameObject.SetActive(toggle);

            UpdateLabelSprite(toggle);
        }

        public float GetRotationTime()
        {
            float div = (Quaternion.Angle(currentSlideCache.slideContentHolder.rotation, Quaternion.identity) / 180f);

            if (div == 0f)
                return 0f;

            return rotationResetSpeed / div;
        }

        public IEnumerator LerpTransformTo(Quaternion target, Vector3 scale, bool tweenScale = true)
        {
            cancelTweens = false;

            float timer = GetRotationTime();

            if (timer == Mathf.Infinity || timer == 0)
            {
                yield return null;
            }
            else
            {
                if (timer > rotationResetSpeed)
                    timer = rotationResetSpeed;

                float startTime = timer;

                Vector3 startScale = currentSlideCache.slideContentHolder.localScale;

                Quaternion startPos = currentSlideCache.slideContentHolder.localRotation;

                if (!tweenScale)
                    currentSlideCache.slideContentHolder.localScale = scale;

                while (timer >= 0f)
                {
                    timer -= Time.deltaTime;

                    if (cancelTweens)
                    {
                        cancelTweens = false;
                        break;
                    }

                    float t = Mathf.InverseLerp(startTime, 0f, timer);

                    currentSlideCache.slideContentHolder.localRotation = Quaternion.Lerp(startPos, target, lerpCurve.Evaluate(t));

                    if (tweenScale && currentSlideCache.slideContentHolder.localScale != scale)
                        currentSlideCache.slideContentHolder.localScale = Vector3.Lerp(startScale, scale, lerpCurve.Evaluate(t));

                    yield return null;
                }
            }
        }

        public void NextSlide()
        {
            if(HasMoreSlides())
            {
                ShowSlide(currentBranch, currentSlide + 1);
            }
            else
            {
                SceneManager.LoadScene(0);
                //MainMenu();
            }
        }

        public void MainMenu()
        {
            Scene scene = SceneManager.GetActiveScene();
            if(scene.name == "Scene_Breezhaler_English")
            {
                SceneManager.LoadScene(0);
            }
            else if(scene.name == "Scene_Breezhaler_Japanese")
            {
                SceneManager.LoadScene(0);
            }
        }

        public void PreviousSlide()
        {
            if(HasLessSlides())
            {
                ShowSlide(currentBranch, currentSlide - 1);
            }
            if(currentSlide == 0)
            {
                rightbuttonTwo.SetActive(true);
            }
        }

        bool HasMoreSlides()
        {
            SlideBranch branch = GetBranch(currentBranch);

            return branch.slides.Length > (currentSlide + 1);
        }

        bool HasLessSlides()
        {
            return currentSlide > 0;
        }

        private void Update()
        {
            if (doFixedMode)
                MoveToCamera();

            /*if(currentSlide == 0)
            {
                rightbuttonTwo.SetActive(true);
            }
            else
            {
                rightbuttonTwo.SetActive(false);
            }*/
        }
          
        /*foreach (Touch touch in Input.touches)
                        {
                            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                            RaycastHit hit = new RaycastHit();
                            if (Physics.Raycast(ray, out hit, 1000.0f))
                            {
                                if (hit.collider.gameObject == this.gameObject)
                                {
                                    Debug.LogWarning("HITTING ON GAME OBJECT ::>>>." + hit.transform.name);
                                    return;
                                }
                            }
                        }*/

        private void LateUpdate()
        {
           if (currentSlideCache == null)
                return;
 
            if(currentSlideCache.isGestureDriven && !currentSlideCache.isScrolling)
            {
                if (Input.touchCount == 2 && !currentSlideCache.isScrolling) // touchCount == 2
                {
                    DetectTouch.Calculate();

                    if (Mathf.Abs(DetectTouch.pinchDistanceDelta) > 0)
                    {
                        //ToggleLabels(false);
                        currentSlideCache.slideContentHolder.localScale = new Vector3(
                            Mathf.Clamp(currentSlideCache.slideContentHolder.localScale.x + (DetectTouch.pinchDistanceDelta * pinchMultiplier), currentSlideCache.minZoom, currentSlideCache.maxZoom),
                            Mathf.Clamp(currentSlideCache.slideContentHolder.localScale.y + (DetectTouch.pinchDistanceDelta * pinchMultiplier), currentSlideCache.minZoom, currentSlideCache.maxZoom),
                            Mathf.Clamp(currentSlideCache.slideContentHolder.localScale.z + (DetectTouch.pinchDistanceDelta * pinchMultiplier), currentSlideCache.minZoom, currentSlideCache.maxZoom)
                            );

                        if (currentSlideCache.labelContent != null)
                        {
                            //currentSlideCache.labelContent.localScale = currentSlideCache.slideContentHolder.localScale;
                        }
                    }

                    if((!arState || currentSlideCache.arFullRotation) && Mathf.Abs(DetectTouch.turnAngleDelta) > 0)
                    {
                        //ToggleLabels(false);
                        currentSlideCache.slideContentHolder.Rotate(GetCameraRelativeForward(), (DetectTouch.turnAngleDelta * rotationMultiplier.z), Space.World);
                    }

                    if (currentSlideCache.labelContent != null)
                    {
                        currentSlideCache.labelContent.rotation = currentSlideCache.slideContentHolder.rotation;
                    }
                }
                else if(Input.touchCount == 1)
                {
                    if (Input.touches[0].phase == TouchPhase.Began)
                    {
                         
                        singleTouchStartPos = Input.touches[0].position;
                        didPassDeadzone = false;
                    }

                    if(!didPassDeadzone && Vector2.Distance(singleTouchStartPos, Input.touches[0].position) > rotationDeadzone)
                    {
                        didPassDeadzone = true;
                    }

                    if (didPassDeadzone)
                    {
                        //ToggleLabels(false);
                        //Debug.Log("Yaw: " + Input.touches[0].deltaPosition.x);
                        currentSlideCache.slideContentHolder.Rotate(Vector3.up, Input.touches[0].deltaPosition.x * rotationMultiplier.x, Space.World);
                    }

                    if (!arState || currentSlideCache.arFullRotation)
                    {
                        if (didPassDeadzone)
                        {
                            //Debug.Log("Roll: " + Input.touches[0].deltaPosition.y);
                            currentSlideCache.slideContentHolder.Rotate(GetCameraRelativeRight(), Input.touches[0].deltaPosition.y * rotationMultiplier.y, Space.World);
                        }
                    }

                    if (currentSlideCache.labelContent != null)
                    {
                        currentSlideCache.labelContent.rotation = currentSlideCache.slideContentHolder.rotation;
                    }
                }

                Vector3 angle = new Vector3(
                    GetNegativeAngle(currentSlideCache.slideContentHolder.localEulerAngles.x),
                    GetNegativeAngle(currentSlideCache.slideContentHolder.localEulerAngles.y),
                    GetNegativeAngle(currentSlideCache.slideContentHolder.localEulerAngles.z)
                    );

                if (currentSlideCache.limitRotation)
                {
                    currentSlideCache.slideContentHolder.localEulerAngles = Vector3.Min(Vector3.Max(angle, currentSlideCache.rotationMin), currentSlideCache.rotationMax);

                    if (currentSlideCache.labelContent != null)
                        currentSlideCache.labelContent.localEulerAngles = currentSlideCache.slideContentHolder.localEulerAngles;
                }
            }  
        }

        float GetNegativeAngle(float angle)
        {
            return angle > 180f ? angle - 360f : angle;
        }

        public Vector3 GetCameraRelativeForward()
        {
            return new Vector3(Camera.main.transform.forward.x, 0f, Camera.main.transform.forward.z).normalized;
        }

        public Vector3 GetCameraRelativeRight()
        {
            return new Vector3(Camera.main.transform.right.x, 0f, Camera.main.transform.right.z).normalized;
        }

        public bool IsFixedMode()
        {
            return PlayerPrefs.GetInt("FixedMode", 1) == 1;
        }

        public void UpdateFixedMode()
        {
            doFixedMode = IsFixedMode();
        }

        //Load Breezhaler Sensor App in Enerzair.
        public void OnBreezhalerSensorClicked()
        {
            SceneManager.LoadSceneAsync("Scene_MainMenu_English");
        }
 

    }
}