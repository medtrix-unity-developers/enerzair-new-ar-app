﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionScreenSpace : MonoBehaviour
{
    public RectTransform target;
    public float distance = 2f;
    public bool doLateUpdate = false;
    public Transform inverseObject;
    public Vector3 arScale = Vector3.one;

    Vector3 startScale;
    Vector3 startRotation;

    private void OnEnable()
    {
        startScale = transform.localScale;
        SetPosition();
    }

    // Update is called once per frame
    void Update()
    {
        if(!doLateUpdate)
            SetPosition();
        //transform.position = new Vector3(transform.position.x, transform.position.y, distance);
    }

    private void LateUpdate()
    {
        if (doLateUpdate)
            SetPosition();
    }

    void SetPosition()
    {
        if (target.gameObject.activeInHierarchy)
        {
            transform.position = Camera.main.ScreenToWorldPoint(new Vector3(target.position.x, target.position.y, distance));

            if (inverseObject != null)
                transform.localScale = new Vector3(startScale.x / inverseObject.localScale.x * arScale.x, startScale.y / inverseObject.localScale.y * arScale.y, startScale.z / inverseObject.localScale.z * arScale.z);
        }
        else
            transform.position = new Vector3(1000f, 1000f, -100f);
    }
}
