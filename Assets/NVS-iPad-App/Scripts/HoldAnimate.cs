﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HoldAnimate : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Animator animator;

    public void OnPointerDown(PointerEventData eventData)
    {
        animator.SetBool("Show", true);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        animator.SetBool("Show", false);
    }
}
