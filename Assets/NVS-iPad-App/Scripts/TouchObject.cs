﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QVM
{
    public class TouchObject : MonoBehaviour
    {
        [Header("Rotation Settings")]
        public Vector2 rotationMultiplier;
        public float rotationDeadzone = 0.02f;

        [Header("Zoom Settings")]
        public float pinchMultiplier = 1f;
        public float minScale = 1f;
        public float maxScale = 1f;

        Vector2 touchStartPos;
        bool didPassDeadzone = false;

        Vector3 startScale;
        Quaternion startRotation;

        private void Awake()
        {
            minScale = transform.localScale.x * minScale;
            maxScale = transform.localScale.x * maxScale;

            startScale = transform.localScale;
            startRotation = transform.localRotation;
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        public void ResetTransform()
        {
            transform.localScale = startScale;
            transform.localRotation = startRotation;
        }

        private void LateUpdate()
        {
            if(Input.touchCount == 2)
            {
                DetectTouch.Calculate();

                if(Mathf.Abs(DetectTouch.pinchDistanceDelta) > 0)
                {
                    transform.localScale = new Vector3(
                        Mathf.Clamp(transform.localScale.x + (DetectTouch.pinchDistanceDelta * pinchMultiplier), minScale, maxScale),
                        Mathf.Clamp(transform.localScale.y + (DetectTouch.pinchDistanceDelta * pinchMultiplier), minScale, maxScale),
                        Mathf.Clamp(transform.localScale.z + (DetectTouch.pinchDistanceDelta * pinchMultiplier), minScale, maxScale)
                    );
                }
            } else if(Input.touchCount == 1)
            {
                if(Input.touches[0].phase == TouchPhase.Began)
                {
                    touchStartPos = Input.touches[0].position;
                    didPassDeadzone = false;
                }

                if(!didPassDeadzone && Vector2.Distance(touchStartPos, Input.touches[0].position) > rotationDeadzone)
                {
                    didPassDeadzone = true;
                }

                if(didPassDeadzone)
                {
                    transform.Rotate(new Vector3(Input.touches[0].deltaPosition.y * rotationMultiplier.x, Input.touches[0].deltaPosition.x * rotationMultiplier.y, 0f));
                }
            }
        }
    }
}