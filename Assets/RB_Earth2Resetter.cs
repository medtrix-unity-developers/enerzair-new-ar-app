﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RB_Earth2Resetter : MonoBehaviour
{
    public GameObject textConferenceMode, parentSlide;

    private void OnEnable()
    {
        if (!textConferenceMode.activeSelf)
            InvokeRepeating("UpdateSlidePosition", 0f, 0.1f);
    }

    private void UpdateSlidePosition()
    {
        if(parentSlide.transform.localPosition.z != -4.7f)
        {
            parentSlide.transform.localPosition = new Vector3(parentSlide.transform.localPosition.x,
                parentSlide.transform.localPosition.y, -4.7f);
        }
        else
        {
            CancelInvoke("UpdateSlidePosition");
        }
    }

    void Start()
    {
       
    }

    void Update()
    {
        
    }

    private void OnDisable()
    {
        if (!textConferenceMode.activeSelf)
        {
            parentSlide.transform.localPosition = new Vector3(parentSlide.transform.localPosition.x,
                parentSlide.transform.localPosition.y, 0f);
        }        
    }
}
