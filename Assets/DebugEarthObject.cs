﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugEarthObject : MonoBehaviour
{
    public GameObject cam, debugEarthObject, slideEarth;
    public Text debugText;
    public Transform globe, globeText;
    private float zValue = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        debugText.text = globe.localPosition.z.ToString();
        if (cam.GetComponent<Camera>().orthographic)
        {
            globe.localPosition = new Vector3(globe.localPosition.x, globe.localPosition.y, 0f);
            globeText.localPosition = new Vector3(globeText.localPosition.x, globeText.localPosition.y, 0f);
        }
        else
        {
            globe.localPosition = new Vector3(globe.localPosition.x, globe.localPosition.y, zValue);
            globeText.localPosition = new Vector3(globeText.localPosition.x, globeText.localPosition.y, zValue);
        }

        if (slideEarth.activeInHierarchy && !cam.GetComponent<Camera>().orthographic)
        {
            debugEarthObject.SetActive(true);
        }
        else
        {
            debugEarthObject.SetActive(false);
        }
    }

    public void ValuePlus()
    {
        zValue += 0.01f;
        //globe.localPosition = new Vector3(globe.localPosition.x, globe.localPosition.y, globe.localPosition.z + 0.01f);
        //globeText.localPosition = new Vector3(globeText.localPosition.x, globeText.localPosition.y, globeText.localPosition.z + 0.01f);
    }

    public void ValueMinus()
    {
        zValue -= 0.01f;
        //globe.localPosition = new Vector3(globe.localPosition.x, globe.localPosition.y, globe.localPosition.z - 0.01f);
        //globeText.localPosition = new Vector3(globeText.localPosition.x, globeText.localPosition.y, globeText.localPosition.z - 0.01f);
    }
}
