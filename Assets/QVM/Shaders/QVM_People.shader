// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "QVM/People"
{
	Properties
	{
		_BaseColour("Base Colour", Color) = (1,1,1,0)
		_FillColour("Fill Colour", Color) = (0,0,0,0)
		_LowVal("Low Val", Range( 0 , 1)) = 0
		_BlockSize("Block Size", Float) = 0.1
		_StartX("Start X", Float) = -1
		_EndX("End X", Float) = 1
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float3 worldPos;
		};

		uniform float _StartX;
		uniform float _EndX;
		uniform float PeopleProgress;
		uniform float _LowVal;
		uniform float _BlockSize;
		uniform float4 _BaseColour;
		uniform float4 _FillColour;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float lerpResult31 = lerp( _StartX , _EndX , (0.0 + (PeopleProgress - _LowVal) * (1.0 - 0.0) / (( _LowVal + _BlockSize ) - _LowVal)));
			float4 ifLocalVar13 = 0;
			if( ase_vertex3Pos.x >= lerpResult31 )
				ifLocalVar13 = _BaseColour;
			else
				ifLocalVar13 = _FillColour;
			o.Albedo = ifLocalVar13.rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16800
-2553;1;2546;1371;1498.904;741.4468;1;True;False
Node;AmplifyShaderEditor.RangedFloatNode;23;-1109.904,-200.4468;Float;False;Property;_BlockSize;Block Size;3;0;Create;True;0;0;False;0;0.1;0.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-1123.904,-330.4468;Float;False;Property;_LowVal;Low Val;2;0;Create;True;0;0;False;0;0;0.502;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;24;-778.9041,-210.4468;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;16;-826.5405,-412.4337;Float;False;Global;PeopleProgress;People Progress;2;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;28;-187.9041,-486.4468;Float;False;Property;_StartX;Start X;4;0;Create;True;0;0;False;0;-1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;29;-192.9041,-409.4468;Float;False;Property;_EndX;End X;5;0;Create;True;0;0;False;0;1;0.12;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;30;-517.9041,-366.4468;Float;True;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;2;-523,103;Float;False;Property;_FillColour;Fill Colour;1;0;Create;True;0;0;False;0;0,0,0,0;0.5647059,0.6980392,0.7843137,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;31;31.09595,-423.4468;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;1;-524,-71;Float;False;Property;_BaseColour;Base Colour;0;0;Create;True;0;0;False;0;1,1,1,0;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PosVertexDataNode;27;-160.9041,-217.4468;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ConditionalIfNode;13;362,-191;Float;True;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;677,-112;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;QVM/People;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;24;0;19;0
WireConnection;24;1;23;0
WireConnection;30;0;16;0
WireConnection;30;1;19;0
WireConnection;30;2;24;0
WireConnection;31;0;28;0
WireConnection;31;1;29;0
WireConnection;31;2;30;0
WireConnection;13;0;27;1
WireConnection;13;1;31;0
WireConnection;13;2;1;0
WireConnection;13;3;1;0
WireConnection;13;4;2;0
WireConnection;0;0;13;0
ASEEND*/
//CHKSM=6264C9A8D250D63291C17AC466382AA024408C22