﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace QVM
{
    public class PlatinumWidget : MonoBehaviour
    {
        [System.Serializable]
        public class WidgetSection
        {
            public Sprite connectionSprite;
            public Button button;

            [TextArea]
            public string circleText;
            //[TextArea]
            //public string populationText;
        }

        public List<WidgetSection> sections;

        public TMPro.TextMeshProUGUI tmpCircle;
        public TMPro.TextMeshProUGUI tmpPopulation;
        public Image connectionImage;

        // Start is called before the first frame update
        void Start()
        {
            SetWidgetIndex(0);
        }

        private void OnEnable()
        {
            SetWidgetIndex(0);
        }

        public void SetWidgetIndex(int index)
        {
            for(int i = 0; i < sections.Count; i++)
            {
                if (i == index)
                {
                    sections[i].button.GetComponent<Animator>().SetBool("ForceSelected", true);
                    connectionImage.sprite = sections[i].connectionSprite;
                    tmpCircle.SetText(sections[i].circleText);
                }
                else
                {
                    sections[i].button.GetComponent<Animator>().SetBool("ForceSelected", false);
                    sections[i].button.GetComponent<Animator>().SetTrigger("Normal");
                }
            }

        }
    }
}