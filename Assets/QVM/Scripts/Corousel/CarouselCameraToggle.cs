﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarouselCameraToggle : MonoBehaviour
{
    //public Camera camera;
    // Start is called before the first frame update

    private void OnEnable()
    {
      Camera.main.orthographic = false;
    }

    // Update is called once per frame
    private void OnDisable()
    {
        Camera.main.orthographic = true;
    }
  
}
