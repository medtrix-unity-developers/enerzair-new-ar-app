﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RB_ButtonInteractionManager : MonoBehaviour
{
    public Sprite activeSprite, inactiveSprite;
    //All the buttons in a particular screen
    public GameObject[] btnObject;
    public bool secondButtonLayerExists;
    public GameObject secondBtnLayerContainer;
    //public Sprite activeOrange, inactiveOrange;
    //public GameObject[] subBtnObject;

    private int t;

    private void OnEnable()
    {
        if (secondButtonLayerExists)
        {
            secondBtnLayerContainer.SetActive(false);
        }
        //this disables the hotspot
        if(btnObject[0].transform.childCount > 2)
            btnObject[0].transform.GetChild(2).gameObject.SetActive(true);

        for (int i = 0; i < btnObject.Length; i++)
        {
            btnObject[i].GetComponent<Button>().interactable = true;
            btnObject[i].transform.GetChild(0).GetComponent<Image>().sprite = inactiveSprite;
            btnObject[i].transform.GetChild(1).GetComponent<TextMeshProUGUI>().color =
                new Color32(50, 50, 50, 255);           
        } 
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(btnObject[0].GetComponent<Button>().image.sprite.name);
        //if (Input.GetKeyDown(KeyCode.A))
        //{
        //    btnObject[t].GetComponent<Button>().image.sprite = activeSprite;
        //}

        //if (Input.GetKeyDown(KeyCode.S))
        //{
        //    btnObject[t].GetComponent<Button>().image.sprite = inactiveSprite;
        //}
    }

    public void OnButtonClick(int btnIndex)
    {
        t = btnIndex;
        if (secondButtonLayerExists)
        {
            secondBtnLayerContainer.SetActive(true);
        }
        //this disables the hotspot
        if (btnObject[0].transform.childCount > 2)
            btnObject[0].transform.GetChild(2).gameObject.SetActive(false);

        for (int i = 0; i < btnObject.Length; i++)
        {
            if (i != btnIndex)
            {
                Debug.Log("InActive");
                btnObject[i].GetComponent<Button>().interactable = true;
                btnObject[i].transform.GetChild(0).GetComponent<Image>().sprite = inactiveSprite;
                btnObject[i].transform.GetChild(1).GetComponent<TextMeshProUGUI>().color =
                    new Color32(50, 50, 50, 255);
            }
            else
            {
                Debug.Log("Active");
                btnObject[i].GetComponent<Button>().interactable = false;
                btnObject[i].transform.GetChild(0).GetComponent<Image>().sprite = activeSprite;
                btnObject[i].transform.GetChild(1).GetComponent<TextMeshProUGUI>().color =
                    new Color32(255, 255, 255, 255);
            }
        }
    }

    //public void OnSubButtonClicked(int subBtnIndex)
    //{

    //}
}
