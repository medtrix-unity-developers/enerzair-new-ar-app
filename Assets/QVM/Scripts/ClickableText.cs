﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using NVS;


public class ClickableText : MonoBehaviour, IPointerClickHandler
{
 
     public SlideManager slidemanager;
     

    public void OnPointerClick(PointerEventData eventData = null)
    {

        Debug.Log("CLICKED OnPointerClick: ");

        var text = GetComponent<TextMeshProUGUI>();
        if (eventData != null && eventData.button == PointerEventData.InputButton.Left)
        {
            int linkIndex = TMP_TextUtilities.FindIntersectingLink(text, Input.mousePosition, null);
            if (linkIndex > -1)
            {
                var linkInfo = text.textInfo.linkInfo[linkIndex];
                var linkId = linkInfo.GetLinkID();
                //var itemData = FindObjectOfType<ItemDataController>().Get(linkId);
                Debug.Log("CLICKED LINK NAME : " + linkInfo.GetLinkID());

                if (linkInfo.GetLinkID() == "ENERZAIR")
                {
                    slidemanager.ShowSlide(4);
                }

                else if (linkInfo.GetLinkID() == "ATECTURA")
                {
                    slidemanager.ShowSlide(11);
                }
                else if (linkInfo.GetLinkID() == "Glycopyrronium")
                {
                    slidemanager.ShowSlide(4);
                }
                else if (linkInfo.GetLinkID() == "Mometasone")
                {
                    slidemanager.ShowSlide(11);
                }
                else if (linkInfo.GetLinkID() == "ClinicalTrials")
                {
                    Application.OpenURL("https://www.clinicaltrials.gov/");
                }


            }
        }
        else
        {
            int linkIndex = TMP_TextUtilities.FindIntersectingLink(text, Input.mousePosition, null);
            if (linkIndex > -1)
            {
                var linkInfo = text.textInfo.linkInfo[linkIndex];
                var linkId = linkInfo.GetLinkID();
                if (linkInfo.GetLinkID() == "ClinicalTrials")
                {
                    Application.OpenURL("https://www.clinicaltrials.gov/");
                }
            }
        }
    }
 
}
