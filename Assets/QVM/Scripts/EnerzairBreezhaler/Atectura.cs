﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class Atectura : MonoBehaviour
{
    public GameObject[] ContentGraphHolder;
    public GameObject[] ContentMediumGraphHolder;

    public Button[] SmallButtons;
    public Button[] SmallMediumButtons;

    public GameObject[] CanvasLabels;
    public GameObject[] CanvasMediumLabels;


    /* public GameObject HighLungLabel;
     public GameObject HighExacerbationLabel;
     public GameObject HighRescueLabel;
     public GameObject MediumLungLabel;
     public GameObject MediumExacerbationLabel;
     public GameObject MediumRescueLabel; */

    public Button HighDoseButton;
    public Button MediumDoseButton;

    public int clickedSmallButtonID = 0;
    public int clickedBigButtonID = 0;

    public Sprite orangeUnselected;
    public Sprite orangeSelected;
    public Sprite GreenUnselected;
    public Sprite GreenSelected;
 

    void OnEnable()
    {
    

    }

    public void OnAtecturaSlideOpen()
    {
        HighDoseButton.interactable = false;
        MediumDoseButton.interactable = true;

        for(int i =0; i < SmallButtons.Length; i++)
        {
                SmallButtons[i].gameObject.SetActive(true);
                SmallMediumButtons[i].gameObject.SetActive(false);
                if (SmallButtons[i].name == "LungFunction_0")
                {
                    SmallButtons[i].interactable = false;
                    SmallButtons[i].transform.GetChild(1).GetComponent<TextMeshProUGUI>().color =
                        new Color32(255, 255, 255, 255);
                }
                else
                {
                    SmallButtons[i].interactable = true;
                    SmallButtons[i].transform.GetChild(1).GetComponent<TextMeshProUGUI>().color =
                        new Color32(50, 50, 50, 255);
                }
              
        }

        HighDoseButton.transform.GetChild(1).GetComponent<TextMeshProUGUI>().color =
             new Color32(255, 255, 255, 255);
        MediumDoseButton.transform.GetChild(1).GetComponent<TextMeshProUGUI>().color =
            new Color32(50, 50, 50, 255);
    }


    public void OnSmallButtonClicked(int clickedButtonID)
    {
        clickedSmallButtonID = clickedButtonID;

        if (clickedBigButtonID == 0)
        {
            for (int i = 0; i < SmallButtons.Length; i++)
            {
                SmallButtons[i].gameObject.SetActive(true);
                SmallMediumButtons[i].gameObject.SetActive(false);

                if (i == clickedSmallButtonID)
                {
                    SmallButtons[i].transform.GetChild(0).GetComponentInChildren<Image>().sprite = orangeSelected;
                    SmallButtons[i].transform.GetChild(1).GetComponent<TextMeshProUGUI>().color =
                        new Color32(255, 255, 255, 255);
                    SmallButtons[i].interactable = false;
                }
                else
                {
                    SmallButtons[i].transform.GetChild(0).GetComponentInChildren<Image>().sprite = orangeUnselected;
                    SmallButtons[i].transform.GetChild(1).GetComponent<TextMeshProUGUI>().color =
                        new Color32(50, 50, 50, 255);
                    SmallButtons[i].interactable = true;
                }
            }
        }
        else if (clickedBigButtonID == 1)
        {
            for (int i = 0; i < SmallMediumButtons.Length; i++)
            {
                SmallButtons[i].gameObject.SetActive(false);
                SmallMediumButtons[i].gameObject.SetActive(true);
                if (i == clickedSmallButtonID)
                {
                    SmallMediumButtons[i].transform.GetChild(0).GetComponentInChildren<Image>().sprite = orangeSelected;
                    SmallMediumButtons[i].transform.GetChild(1).GetComponent<TextMeshProUGUI>().color = new Color32(255, 255, 255, 255);
                    SmallMediumButtons[i].interactable = false;
                }
                else
                {
                    SmallMediumButtons[i].transform.GetChild(0).GetComponentInChildren<Image>().sprite = orangeUnselected;
                    SmallMediumButtons[i].transform.GetChild(1).GetComponent<TextMeshProUGUI>().color = new Color32(50, 50, 50, 255);
                    SmallMediumButtons[i].interactable = true;
                }
            }
        }
    }




    public void OnBigButtonClicked(int clickedButtonID)
    {
        clickedBigButtonID = clickedButtonID;
        Debug.Log("CLICKED :>>>" + clickedBigButtonID + "clickedSmallButtonID:" + clickedSmallButtonID);

        //Big Buttons:
        if(clickedButtonID == 0)
        {
            HighDoseButton.interactable = false;
            MediumDoseButton.interactable = true;
            HighDoseButton.transform.GetChild(0).GetComponent<Image>().sprite = GreenSelected;
            MediumDoseButton.transform.GetChild(0).GetComponent<Image>().sprite = GreenUnselected;

            HighDoseButton.transform.GetChild(1).GetComponent<TextMeshProUGUI>().color =
                new Color32(255, 255, 255, 255);
            MediumDoseButton.transform.GetChild(1).GetComponent<TextMeshProUGUI>().color =
                new Color32(50, 50, 50, 255);
        }
        else
        {
            HighDoseButton.interactable = true;
            MediumDoseButton.interactable = false;
            MediumDoseButton.transform.GetChild(1).GetComponent<TextMeshProUGUI>().color =
                new Color32(255, 255, 255, 255);
            HighDoseButton.transform.GetChild(1).GetComponent<TextMeshProUGUI>().color =
                new Color32(50, 50, 50, 255);
            HighDoseButton.transform.GetChild(0).GetComponent<Image>().sprite = GreenUnselected;
            MediumDoseButton.transform.GetChild(0).GetComponent<Image>().sprite = GreenSelected;
        }




        // Small buttons
        if (clickedButtonID == 0)
        {
            for (int i = 0; i < SmallButtons.Length; i++)
            {
                 SmallButtons[i].gameObject.SetActive(true);
                 SmallMediumButtons[i].gameObject.SetActive(false);
 
                if (i ==  0) // clickedSmallButtonID
                {
                    SmallButtons[i].transform.GetChild(0).GetComponentInChildren<Image>().sprite = orangeSelected;
                    SmallButtons[i].transform.GetChild(1).GetComponent<TextMeshProUGUI>().color = new Color32(255, 255, 255, 255);
                    SmallButtons[i].interactable = false;
                } 
                else
                {
                    SmallButtons[i].transform.GetChild(0).GetComponentInChildren<Image>().sprite = orangeUnselected;
                    SmallButtons[i].transform.GetChild(1).GetComponent<TextMeshProUGUI>().color = new Color32(50, 50, 50, 255);
                    SmallButtons[i].interactable = true;
                }
            }
        }
        else if(clickedButtonID == 1)
        {
            for (int i = 0; i < SmallMediumButtons.Length; i++)
            {
                SmallButtons[i].gameObject.SetActive(false);
                SmallMediumButtons[i].gameObject.SetActive(true);
                if (i == 0) // clickedSmallButtonID
                {
                    SmallMediumButtons[i].transform.GetChild(0).GetComponentInChildren<Image>().sprite = orangeSelected;
                    SmallMediumButtons[i].transform.GetChild(1).GetComponent<TextMeshProUGUI>().color = new Color32(255, 255, 255, 255);
                    SmallMediumButtons[i].interactable = false;
                }
                else
                {
                    SmallMediumButtons[i].transform.GetChild(0).GetComponentInChildren<Image>().sprite = orangeUnselected;
                    SmallMediumButtons[i].transform.GetChild(1).GetComponent<TextMeshProUGUI>().color = new Color32(50, 50, 50, 255);
                    SmallMediumButtons[i].interactable = true;
                }
            }
        }


        //ContentGraphHolder 
        if (clickedButtonID == 0)
        {
            for (int i = 0; i < ContentGraphHolder.Length; i++)
            {
                ContentGraphHolder[i].gameObject.SetActive(true);
                ContentMediumGraphHolder[i].gameObject.SetActive(false);

         
                    if (i == clickedSmallButtonID)
                    {
                        ContentGraphHolder[i].gameObject.SetActive(true);
                    }
                    else
                    {
                        ContentGraphHolder[i].gameObject.SetActive(false);
                    }
             }
           
        }
        else if (clickedButtonID == 1)
        {
            for (int i = 0; i < ContentMediumGraphHolder.Length; i++)
            {
                ContentGraphHolder[i].gameObject.SetActive(false);
                ContentMediumGraphHolder[i].gameObject.SetActive(true);
              
                    if (i == clickedSmallButtonID)
                    {
                        ContentMediumGraphHolder[i].gameObject.SetActive(true);
                    }
                    else
                    {
                        ContentMediumGraphHolder[i].gameObject.SetActive(false);
                    }
                  
            }
        }


        //CanvasLabels
        if (clickedButtonID == 0)
        {
            for (int i = 0; i < CanvasLabels.Length; i++)
            {
                CanvasLabels[i].gameObject.SetActive(true);
                CanvasMediumLabels[i].gameObject.SetActive(false);
                if (i == clickedSmallButtonID)
                    {
                        CanvasLabels[i].gameObject.SetActive(true);
                    }
                    else
                    {
                        CanvasLabels[i].gameObject.SetActive(false);
                    }
                 
            }
        }
        else if (clickedButtonID == 1)
        {
            for (int i = 0; i < CanvasMediumLabels.Length; i++)
            {
                CanvasLabels[i].gameObject.SetActive(false);
                CanvasMediumLabels[i].gameObject.SetActive(true);
                if (i == clickedSmallButtonID)
                    {
                        CanvasMediumLabels[i].gameObject.SetActive(true);
                    }
                    else
                    {
                        CanvasMediumLabels[i].gameObject.SetActive(false);
                    }
            }
        }

         


    }
}
