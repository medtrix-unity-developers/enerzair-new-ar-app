﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnerzairBreezhaler : MonoBehaviour
{
    /*public GameObject ModerateGraph;
    public GameObject SevereGraph;
    public GameObject HumanCoughBody;
    public GameObject ModerateContent;
    public GameObject SevereContent;
    public GameObject StopWatch; */

    public GameObject[] enabledObjects;
    public GameObject[] disabledObjects;



    // Start is called before the first frame update
    void OnEnable()
    {
        //Debug.Log("Coming to check on start....");
        //HumanCoughBody.SetActive(true);
        //SevereGraph.SetActive(false);
        //ModerateGraph.SetActive(false);
        //ModerateContent.SetActive(true);
        //SevereContent.SetActive(false);

        /*if(StopWatch != null)
        {
            StopWatch.SetActive(false);
        } */

        foreach(GameObject enabObjects in enabledObjects)
        {
            enabObjects.SetActive(true);
            if(enabObjects.GetComponent<Animator>())
                enabObjects.GetComponent<Animator>().enabled = false;
        }
        foreach (GameObject disabObjects in disabledObjects)
        {
            disabObjects.SetActive(false);
        }


    }

    void OnDisable()
    {
        foreach (GameObject enabObjects in enabledObjects)
        {
            enabObjects.SetActive(true);
        }
        foreach (GameObject disabObjects in disabledObjects)
        {
            disabObjects.SetActive(false);
        }
    }

}
