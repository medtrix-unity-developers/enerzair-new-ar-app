﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QVM
{
    public class PersonSetup : MonoBehaviour
    {
        public List<GameObject> people;
        //public List<GameObject> people2;

        [Range(0f, 1f)]
        public float progress = 0f;

        // Start is called before the first frame update
        void Start()
        {
            float blockSize = 1f / (float)people.Count;

            for(int i = 0; i < people.Count; i++)
            {
                foreach(Renderer r in people[i].GetComponentsInChildren<Renderer>())
                {
                    r.material.SetFloat("_LowVal", i * blockSize);
                    r.material.SetFloat("_BlockSize", blockSize);
                }
            }
        }

        // Update is called once per frame
        public void Update()
        {
            Shader.SetGlobalFloat("PeopleProgress", progress);
        }
    }
}