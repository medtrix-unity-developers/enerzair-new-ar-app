﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace QVM
{
    public class ButtonEvents : MonoBehaviour
    {
        public UnityEvent onHighlight;
        public UnityEvent onUnhighlight;
        public UnityEvent onClick;

        private void OnMouseEnter()
        {
            if(onHighlight != null)
                onHighlight.Invoke();
        }

        private void OnMouseExit()
        {
            
        }
    }
}