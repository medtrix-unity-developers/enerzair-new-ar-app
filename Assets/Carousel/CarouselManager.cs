﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarouselManager : MonoBehaviour
{

    private float angleY;
    private bool canSwipe;
    private float mousePositionStartX;
    private float mousePositionEndX;
    private float dragAmount;
    private float screenPosition;
    private float lastScreenPosition;
    public int swipeThrustHold = 30;
    public float swipeTime = 0.5f;
    public GameObject carouselContainer;

    private void OnEnable()
    {
        //angleY = 0f;
        //LeanTween.rotate(carouselContainer, new Vector3(0, angleY, 0), 0f);
    }

    void Start()
    {
        
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            canSwipe = true;
            mousePositionStartX = Input.mousePosition.x;
        }


        if (Input.GetMouseButton(0))
        {
            if (canSwipe)
            {
                mousePositionEndX = Input.mousePosition.x;
                dragAmount = mousePositionEndX - mousePositionStartX;
                screenPosition = lastScreenPosition + dragAmount;
            }
        }

        if (Mathf.Abs(dragAmount) > swipeThrustHold && canSwipe)
        {
            canSwipe = false;
            lastScreenPosition = screenPosition;
            
            if (dragAmount < 0)
            {
                angleY += 60f;
                LeanTween.rotateLocal(carouselContainer, new Vector3(0, angleY, 0), swipeTime);
                if (Mathf.Abs(angleY) == 360)
                    angleY = 0;
            }
            else if(dragAmount > 0)
            {
                angleY -= 60f;
                LeanTween.rotateLocal(carouselContainer, new Vector3(0, angleY, 0), swipeTime);
                if (Mathf.Abs(angleY) == 360)
                    angleY = 0;
            }
        }
    }
}
